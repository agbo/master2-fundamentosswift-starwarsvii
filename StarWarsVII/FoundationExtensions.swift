//
//  FoundationExtensions.swift
//  StarWarsVII
//
//  Created by Fernando Rodríguez Romero on 20/11/15.
//  Copyright © 2015 KeepCoding. All rights reserved.
//

import Foundation

extension NSBundle{
    
    func URLForResource(fileName: String)->NSURL?{
        
        // deberes para casa: gestionar errores en caso de
        // no sea del tipo nombre.extension
        let tokens = fileName.componentsSeparatedByString(".")
        
        return self.URLForResource(tokens.first,
            withExtension: tokens.last)
        
    }
}