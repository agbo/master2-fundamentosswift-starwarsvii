//
//  StarWarsUniverse.swift
//  StarWarsVII
//
//  Created by Fernando Rodríguez Romero on 24/11/15.
//  Copyright © 2015 KeepCoding. All rights reserved.
//

import UIKit


// Carta a los Reyes Magos:






class StarWarsUniverse {
    
    //MARK: - Private Interface
    
    // Guardo los personajes en un diccionario cuyas claves son las afiliaciones
    // y los valores son arrays de StarWarsCharacters
    private var characters : [StarWarsAffiliation:[StarWarsCharacter]]
    
    //MARK: - Initialization
    init(arrayOfCharacters: [StarWarsCharacter]){
    
        // Inicializamos el diccionario vacio
        characters = Dictionary<StarWarsAffiliation, Array<StarWarsCharacter>>()
        
        for each in StarWarsAffiliation.cases(){
            characters[each] = Array<StarWarsCharacter>()
        }
        
        // Patearse los personajes, y según su affiliación,
        // los metemos en un sitio u otro
        for character in arrayOfCharacters{
            
            // Según la afiliación lo metemos en un sitio u otro
            let aff = character.affiliation
            
            characters[aff]?.append(character)
            
        }
        
        
        
    }
    
    
    
    
    //MARK: - Public interface
    
    // total de personajes
    var countCharacters: Int{
        get{
            var total = 0
            for affiliation in StarWarsAffiliation.cases(){
                total = total + countCharacters(affiliation)
            }
            return total
        }
    }
    
    // El total de las afiliaciones
    var countAffiliations: Int{
        get{
            return StarWarsAffiliation.countAffiliations()
        }
    }
    
    // Total de personajes para cada afiliación
    func countCharacters(inAffiliation: StarWarsAffiliation) ->Int{
        // Obtengo el array de la afiliación y 
        // le pregunto cual es su tamaño
        if let chars = characters[inAffiliation]{
            return chars.count
        }
        return 0
    
    }
    
    // El nombre de una afiliación
    func affiliationName(atIndex: Int) -> String{
        
        // todo frr: averiuar por qué no deja indexar las keys
        // directamente.
        // pa los demás:
        // a) Asegurarse que el array está siempre ordenado
        // b) Lanzar un error si el indice está fuera del arango
        let affiliations = Array(characters.keys)
        let aff = affiliations[atIndex]
        
        return StarWarsAffiliation.nameOfAffiliation(aff)
    }
    
    // El personaje n de la afiliación m
    subscript(idx: Int, inAffiliation aff: StarWarsAffiliation) ->StarWarsCharacter?{
        get{
            if let chars = characters[aff]{
                return chars[idx]
            }
            return nil
        }
    }
    
}








